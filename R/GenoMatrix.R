#' Data frame that contains genotye data
#'
#' This is useful when you have dataset the includes genotype data in certain columns.
#'
#' @name GenoMatrix
#' @param x The data.frame or matrix to wrap over.
#' @param genocols integer. specifies which columns are genotypic data.
#' @return A GenoMatrix object.
GenoMatrix(x, genocols) %::% data.frame : integer : .
GenoMatrix(x, genocols) %when% {
} %as% {
    if(! length(genocols) > 0) {
        stop("gencols is empty.")
    }
    list(data = x, genocols = genocols)
}

# allow specification of gencols by colnames
GenoMatrix(x, genocols) %::% data.frame : character : .
GenoMatrix(x, genocols) %as% {
    GenoMatrix(x, which(colnames(x) %in% genocols))
}


# allow a HaploMatrix to be also a GenoMatrix
GenoMatrix(x, genocols) %::% HaploMatrix : . : .
GenoMatrix(x, genocols) %as% {
    res = GenoMatrix(x$data, genocols)
    res$haplocols = x$haplocols
    res
}



#' Get genotype data from a GenoMatrix
#'
#' A helper function for extraction of genotype data from a GenoMatrix.
#'
#' @name gdata
#' @param g GenoMatrix object.
#' @return A data.frame containing the genotype data.
gdata(g) %::% GenoMatrix : data.frame
gdata(g) %as% {
    g$data[, g$genocols, drop = FALSE]
}

#' Get SNP names from a GenoMatrix
#'
#' A helper function for extraction of SNP names from a GenoMatrix.
#'
#' @name genoNames
#' @param x GenoMatrix object.
#' @return A character vector containing the SNP names
genoNames(x) %::% GenoMatrix : character
genoNames(x) %as% {
    colnames(x$data)[x$genocols]
}

#' Print a GenoMatrix object
#'
#' A helper function for pretty print a GenoMatrix object
#'
#' @name lprint
#' @param x GenoMatrix object.
lprint(x) %::% GenoMatrix : .
lprint(x) %as% {
    message("Genotype data/matrix:")
    print(head(x$data))
    message("...")
    message(sprintf("Looking at %d SNPs for %d observations.",
                    length(x$genocols),
                    nrow(x$data)))
    invisible(NULL)
}



